'''
Generates pictures of Semitoric Fans for a Given Complexity
'''

import argparse
import matplotlib.pyplot as plt

def generate_semitoric_fans(complexity):
    u0 = (0, -1)
    u1 = (1, 0)
    u2 = (complexity, 1)
    u3 = (-1, 0)

    X = []
    Y = []
    U = []
    V = []

    for n in range(0, complexity):
        if n == 0:
            X.append(0)
            Y.append(0)
            U.append(u0[0])
            V.append(u0[1])
        elif n == 1:
            X.append(0)
            Y.append(0)
            U.append(u1[0])
            V.append(u1[1])
        elif n == 2:
            X.append(0)
            Y.append(0)
            U.append(u2[0])
            V.append(u2[1])
        elif n == 3:
            X.append(0)
            Y.append(0)
            U.append(u3[0])
            V.append(u3[1])
        else:
            X.append(0)
            Y.append(0)
            U.append((0 - complexity) + n)
            V.append(-1)

    fig, ax = plt.subplots()
    ax.axis('equal')

    ax.quiver(X, Y, U, V, angles='xy', scale_units='xy',
              scale=1, linewidth=1)

    x_min = 0 - complexity - 2
    x_max = complexity + 2
    y_min = x_min
    y_max = x_max
    ax.axis([x_min, x_max, y_min, y_max])
    plt.show()

def test():
    fig, ax = plt.subplots()
    ax.axis('equal')

    X = [0, 0, 0]
    Y = [0, 0, 0]
    U = [3, 1, -1]
    V = [1, 0, -1]

    ax.quiver(X, Y, U, V, angles='xy', scale_units='xy', scale=1, linewidth=1)

    ax.axis([-4, 4, -4, 4])

    plt.show()

def main():
    parser = argparse.ArgumentParser(description="Generate Semitoric Fans for a Given Complexity")
    parser.add_argument('-c', '--complexity', type=int, help="Integer for representing complexity")
    args = parser.parse_args()

    if args.complexity is not None:
        generate_semitoric_fans(args.complexity)
    else:
        parser.print_help()

if __name__ == '__main__':
    main()
